README

# 이 프로젝트는

JPA Study 프로젝트 중 기술 블로그 Backend_Admin_Service 입니다.

[우아한형제들 기술블로그](https://techblog.woowahan.com/) 를 참고하여 개발하였습니다.

프로젝트 구성은 회원관리, 게시판관리, 카테고리관리 페이지가 있습니다.

## 기술 사양
### 개발 환경

1. 개발언어: JAVA , JDK 1.8
2. 사용기술 : [Spring Data JPA](https://spring.io/projects/spring-data-jpa), [Thymeleaf](https://www.thymeleaf.org/)
3. DB : [H2 DataBase](https://www.h2database.com/html/main.html)
4. IDE : [IntelliJ](https://www.jetbrains.com/ko-kr/idea/)
5. 형상관리 : [Git](https://git-scm.com/)

### DataBase

Erd 및 테이블 정의서는 /doc 폴더에 있습니다.

![ERD](./doc/erd.png)