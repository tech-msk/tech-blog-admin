(function($){
    'use strict';

    if(typeof window.ui === 'undefined'){
        var front = window.front = {}
    }
    front.makeTable = function (header, dataKey, list, tableSelector) {
        let html = '<div class="col-sm-12">'
                 + '<table id="example2" class="table table-bordered table-hover dataTable dtr-inline" aria-describedby="example2_info">'
                 + '<thead>'
                 + '<tr>';

        for (let i = 0; i < header.length; i++) {
            html += '<th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">'+header[i]+'</th>';
        }

        html += '</tr>'
              + '</thead>'
              + '<tbody>';

        for (let i = 0; i < list.length; i++) {
            for (let j = 0; j < dataKey.length; j++) {
                html += '<tr>'
                      + '<td>'+list[i][dataKey[j]]+'</td>'
                      + '</tr>';
            }
        }

        html += '</tbody>'
              + '</table>'
              + '</div>';

        document.getElementById(tableSelector).innerHTML = html;
    };

    front.makePageNation = function (response, pagenationSelector, getListCallFn) {
        let startNum = response.page * response.size;
        let endNum = startNum + response.size;
        let maxPage = Math.ceil(response.totalCount / response.size);
        let previousDisabled = response.page === 1 ? ' disabled' : '';
        let nextDisabled = response.page === maxPage ? ' disabled' : '';

        let text = 'Showing ' + startNum + 'to ' + endNum + ' of ' + response.totalCount + ' entries'

        let html = '<div class="col-sm-12 col-md-5">'
                 + '<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">' + text + '</div>'
                 + '</div>'
                 + '<div class="col-sm-12 col-md-7">'
                 + '<div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">'
                 + '<ul class="pagination">'
                 + '<li class="paginate_button page-item previous'+previousDisabled+'" id="example2_previous">'
                 + '<a href="'+ getListCallFn +'(1)" aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>'
                 + '</li>';
        for (let i = 1; i <maxPage+1; i++) {
            let isActive = response.page === i ? ' active' : '';
            html += '<li class="paginate_button page-item '+isActive+'">'
                  + '<a href="'+ getListCallFn +'('+i+')" aria-controls="example2" data-dt-idx="'+i+'" tabindex="0" class="page-link">'+i+'</a>'
                  + '</li>';
        }
        html   += '<li class="paginate_button page-item next'+nextDisabled+'" id="example2_next">'
                 + '<a href="'+ getListCallFn +'('+maxPage+')" aria-controls="example2" data-dt-idx="'+maxPage+'" tabindex="0" class="page-link">Next</a>'
                 + '</li>'

        document.getElementById(pagenationSelector).innerHTML = html;
    };

    front.callApi = function (type, url, data, dataType) {
        let prom = $.Deferred();
        $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: dataType || 'json',
            cache: false,
            success: function (result) {
                prom.resolve(result);
            },
            beforeSend : function() {
            },
            error: function () {
                prom.reject.apply(null, arguments);
            },
            complete: function () {
            }
        });
        return prom;
    };

    front.get = function (url, data, dataType) {
        front.callApi("get", url, data, dataType);
    }

    front.post = function (url, data, dataType) {
        front.callApi("post", url, data, dataType);
    }

    front.put = function (url, data, dataType) {
        front.callApi("put", url, data, dataType);
    }

    front.delete = function (url, data, dataType) {
        front.callApi("delete", url, data, dataType);
    }

})(jQuery);