package com.tech.blog.admin.repository;

import com.tech.blog.admin.domain.dto.request.BoardRequest;
import com.tech.blog.admin.domain.dto.response.BoardResponse;
import com.tech.blog.admin.domain.entity.BoardEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The interface Board page repository.
 */
@Repository
public interface BoardQueryRepository extends JpaRepository<BoardEntity, Long> {

    /**
     * 게시판 목록 조회
     *
     * @param boardRequest the board request
     * @param pageable     the pageable
     * @return the page
     */
    @Query(value = "select b from BoardEntity b where b.useYn = 'Y' and b.delYn = 'N'"
            , countQuery = "select count(b) from BoardEntity b where b.useYn = 'Y' and b.delYn = 'N'")
    Page<BoardEntity> findAll(BoardRequest boardRequest, Pageable pageable);

}
