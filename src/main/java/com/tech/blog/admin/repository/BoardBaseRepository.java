package com.tech.blog.admin.repository;

import com.tech.blog.admin.domain.entity.BoardEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

/**
 * The type Board base repository.
 */
@Repository
@RequiredArgsConstructor
@Transactional
public class BoardBaseRepository {

    private final EntityManager em;

    public void save(BoardEntity boardEntity) {
        em.persist(boardEntity);
    }

    @Transactional(readOnly = true)
    public void update(BoardEntity boardEntity) {
        em.merge(boardEntity);
    }

    @Transactional(readOnly = true)
    public void delete(Long bbsSno) {
        BoardEntity findBoardEntity = findOne(bbsSno);
        findBoardEntity.remove();
        em.merge(findBoardEntity);
    }

    public BoardEntity findOne(Long bbsSno) {
        return em.find(BoardEntity.class, bbsSno);
    }

    /**
     * 게시글 전체 삭제(초기화때만 사용)
     */
    public void deleteAllBoard() {
        em.createQuery("delete from BoardEntity b where 1=1").executeUpdate();
    }


}
