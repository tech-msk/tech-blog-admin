package com.tech.blog.admin.controller;

import com.tech.blog.admin.domain.dto.request.BoardRequest;
import com.tech.blog.admin.domain.dto.response.BoardResponse;
import com.tech.blog.admin.domain.entity.BoardEntity;
import com.tech.blog.admin.domain.model.EmptyResponse;
import com.tech.blog.admin.domain.model.PageWrapper;
import com.tech.blog.admin.domain.model.ResponseModel;
import com.tech.blog.admin.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * The type Board controller.
 */
@RestController
@RequiredArgsConstructor
public class BoardController {
    private final BoardService blogService;

    /**
     * 게시판 화면 조회
     *
     * @param modelAndView the model and view
     * @return ModelAndView the list view
     */
    @GetMapping("/board/list")
    public ModelAndView getListView(ModelAndView modelAndView) {
        return blogService.getListView(modelAndView);
    }

    /**
     * 게시판 목록 조회
     *
     * @param boardRequest the board request
     * @return ResponseEntity<ResponseModel<PageWrapper<BoardResponse> >> the list
     */
    @GetMapping("/api/board")
    public ResponseEntity<ResponseModel<PageWrapper<BoardResponse>>> getList(BoardRequest boardRequest) {
        return blogService.getList(boardRequest);
    }

    /**
     * 게시글 상세 조회
     *
     * @param bbsSno the bbs sno
     * @return ResponseEntity<ResponseModel<BoardResponse> > the detail
     */
    @GetMapping("/api/board/{bbsSno}}")
    public ResponseEntity<ResponseModel<BoardResponse>> getDetail(@PathVariable Long bbsSno) {
        return blogService.getDetail(bbsSno);
    }

    /**
     * 게시글 등록
     *
     * @param boardEntity the board entity
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    @PostMapping("/api/board")
    public ResponseEntity<ResponseModel<EmptyResponse>> saveBoard(BoardEntity boardEntity) {
        return blogService.saveBoard(boardEntity);
    }

    /**
     * 게시글 수정
     *
     * @param boardEntity the board entity
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    @PutMapping("/api/board")
    public ResponseEntity<ResponseModel<EmptyResponse>> updateBoard(BoardEntity boardEntity) {
        return blogService.updateBoard(boardEntity);
    }

    /**
     * 게시글 삭제
     *
     * @param bbsSno the bbs sno
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    @DeleteMapping("/api/board/{bbsSno}}")
    public ResponseEntity<ResponseModel<EmptyResponse>> deleteBoard(@PathVariable Long bbsSno) {
        return blogService.deleteBoard(bbsSno);
    }

}
