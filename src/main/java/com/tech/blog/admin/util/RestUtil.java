package com.tech.blog.admin.util;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tech.blog.admin.domain.model.BaseRequest;
import com.tech.blog.admin.domain.model.PageWrapper;
import com.tech.blog.admin.domain.model.ResponseModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
@RequiredArgsConstructor
public class RestUtil {
    private static final ObjectMapper objectMapper; // ObjectMapper
    private static final ModelMapper modelMapper; // ObjectMapper

    private static final String SUC_CODE = "SUC001";
    private static final String SUC_MSG = "처리가 완료되었습니다.";
    private static final String ERR_CODE = "ERR001";
    private static final String ERR_MSG = "오류가 발생했습니다.";

    static { // ObjectMapper, ModelMapper 생성 및 설정
        objectMapper = new ObjectMapper()
                /*
                 * convertValue 를 사용할 때
                 * fromValue 객체에는 있지만 toValueType 객체에는 없는 필드가 있으면 에러가 발생합니다.
                 * 해당 에러를 발생하지 않기 위해서
                 * fromValue 객체에는 있지만 toValueType 객체에는 없는 필드를 무시하도록 설정
                 */
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        modelMapper = new ModelMapper();
    }

    public static <T> ResponseEntity<ResponseModel<T>> ok(final T body) {
        return ok(body, SUC_CODE, SUC_MSG);
    }

    public static <T> ResponseEntity<ResponseModel<T>> ok(final T body, final String code, final String message) {

        ResponseModel<T> model = new ResponseModel<T>();

        model.setCode(code);
        model.setMessage(message);
        model.setData(body);

        return ResponseEntity
                .ok()
                .body(model);
    }

    public static ResponseEntity<?> error() {
        return error(BAD_REQUEST, ERR_CODE, ERR_MSG);
    }

    public static <T> ResponseEntity<?> error(final HttpStatus status, final String code, final String message) {
        return ResponseEntity
                .status(status)
                .body(objectMapper.valueToTree(
                        ResponseModel
                                .builder()
                                .code(code)
                                .message(message)
                                .data(null)
                                .build()
                ));
    }

    public static <B, A> A convert(final B before, final Class<A> after) {
        return objectMapper.convertValue(before, after);
    }

    public static <B, A> List<A> convert(final List<B> before, final Class<A> after) {
        return before.stream().map(item -> convert(item, after)).collect(Collectors.toList());
    }

    public static Map<String, Object> toMap (final String jsonString) {
        try {
            return objectMapper.readValue(jsonString, Map.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getClientIp (final HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");

        if (null == ip) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

    public static <T extends BaseRequest> PageRequest getPageable(final T input) {
        int page = ofNullable(input.getPage()).orElse(1);
        int size = ofNullable(input.getSize()).orElse(10);
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "regDtm"));
        return pageRequest;
    }

    public static <T> PageWrapper<T> convertPageWrapper(final Page<T> body) {
        return (PageWrapper<T>) PageWrapper.builder().pageObject((Page<Object>) body).build();
    }

    public static <B, A> A convertEntityToDto(final B before, final Class<A> after) {
        return modelMapper.map(before, after);
    }

    public static <B, A> List<A> convertEntityToDto(final List<B> before, final Class<A> after) {
        return before.stream().map(item -> convertEntityToDto(item, after)).collect(Collectors.toList());
    }
}
