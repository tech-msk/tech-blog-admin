package com.tech.blog.admin.service;

import com.tech.blog.admin.domain.dto.request.BoardRequest;
import com.tech.blog.admin.domain.dto.response.BoardResponse;
import com.tech.blog.admin.domain.entity.BoardEntity;
import com.tech.blog.admin.domain.model.EmptyResponse;
import com.tech.blog.admin.domain.model.PageWrapper;
import com.tech.blog.admin.domain.model.ResponseModel;
import com.tech.blog.admin.repository.BoardBaseRepository;
import com.tech.blog.admin.repository.BoardQueryRepository;
import com.tech.blog.admin.util.RestUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;

/**
 * The type Board service.
 */
@Service
@RequiredArgsConstructor
public class BoardService {

    private final BoardBaseRepository baseRepository;
    private final BoardQueryRepository queryRepository;

    /**
     * 게시판 화면 조회
     *
     * @param modelAndView the model and view
     * @return ModelAndView the list view
     */
    public ModelAndView getListView(ModelAndView modelAndView) {
        modelAndView.setViewName("board/boardList");
        return modelAndView;
    }

    /**
     * 게시판 목록 조회
     *
     * @param boardRequest the board request
     * @return ResponseEntity<ResponseModel<PageWrapper<BoardResponse>>> the list
     */
    public ResponseEntity<ResponseModel<PageWrapper<BoardResponse>>> getList(BoardRequest boardRequest) {
        return RestUtil.ok(
                RestUtil.convertPageWrapper(
                        queryRepository.findAll(boardRequest, RestUtil.getPageable(boardRequest))
                )
        );
    }

    /**
     * 게시글 상세 조회
     *
     * @param bbsSno the bbs sno
     * @return ResponseEntity<ResponseModel<BoardResponse>> the detail
     */
    public ResponseEntity<ResponseModel<BoardResponse>> getDetail(Long bbsSno) {
        return RestUtil.ok(RestUtil.convertEntityToDto(baseRepository.findOne(bbsSno), BoardResponse.class));
    }

    /**
     * 게시글 등록
     *
     * @param boardEntity the board entity
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    public ResponseEntity<ResponseModel<EmptyResponse>> saveBoard(BoardEntity boardEntity) {
        baseRepository.save(boardEntity);
        return RestUtil.ok(null);
    }

    /**
     * 게시글 수정
     *
     * @param boardEntity the board entity
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    public ResponseEntity<ResponseModel<EmptyResponse>> updateBoard(BoardEntity boardEntity) {
        baseRepository.update(boardEntity);
        return RestUtil.ok(null);
    }

    /**
     * 게시글 삭제
     *
     * @param bbsSno the bbs sno
     * @return ResponseEntity<ResponseModel<EmptyResponse>>
     */
    public ResponseEntity<ResponseModel<EmptyResponse>> deleteBoard(Long bbsSno) {
        baseRepository.delete(bbsSno);
        return RestUtil.ok(null);
    }

    /**
     * 게시판 데이터 초기화.
     */
//    @PostConstruct
    public void initializing() {
        baseRepository.deleteAllBoard();
        for (int i = 0; i < 100; i++) {
            BoardEntity boardEntity = BoardEntity.builder()
                    .title("게시판" + i)
                    .cn("내용" + i)
                    .build();
            baseRepository.save(boardEntity);
        }
    }
}
