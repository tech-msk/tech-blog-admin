package com.tech.blog.admin.domain.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

@Getter @Setter
public class BaseRequest extends Pageable {
    private Integer page = 1;
    private Integer size = 10;
}
