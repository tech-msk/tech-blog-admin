package com.tech.blog.admin.domain.entity;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CATEGORY")
@Getter
public class CategoryEntity {

    @Id
    @Column(name = "ctgry_sno")
    private Long ctgrySno;

    @Column(name = "ctgry_nm")
    private String ctgryNm;

    @Column(name = "upper_ctgry_sno")
    private Long upperCtgrySno;

}
