package com.tech.blog.admin.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Objects;

import static java.util.Optional.ofNullable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class PageWrapper<T> {

    @JsonIgnore
    private Page<T> pageObject;

    private List<T> list;
    private int page;
    private int size;
    private Long totalCount;

    @Builder
    public PageWrapper(Page<T> pageObject) {
        if (!Objects.isNull(pageObject)) {
            pageableToPageWrapper(pageObject);
        }
    }

    private void pageableToPageWrapper(Page<T> pageObject) {
        this.list = pageObject.getContent();
        this.page = ofNullable(pageObject.getPageable().getPageNumber()).orElse(1);
        this.size = pageObject.getPageable().getPageSize();
        this.totalCount = pageObject.getTotalElements();
    }

}
