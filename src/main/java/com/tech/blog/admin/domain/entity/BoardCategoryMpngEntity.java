package com.tech.blog.admin.domain.entity;

import lombok.Getter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Embeddable
@Table(name = "TB_BOARD_CATEGORY_MPNG")
@Getter
public class BoardCategoryMpngEntity {

    @ManyToOne
    @JoinColumn(name = "bbs_sno")
    private BoardEntity boardEntity;

    @ManyToOne
    @JoinColumn(name = "ctgry_sno")
    private CategoryEntity categoryEntity;

}
