package com.tech.blog.admin.domain.entity;

import com.tech.blog.admin.domain.entity.base.BaseEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TB_BOARD")
@SequenceGenerator(
        name="BOARD_SEQ_GEN", // 시퀀스 제너레이터 이름
        sequenceName="BOARD_SEQ", // 시퀀스 이름
        initialValue=1,// 시작값
        allocationSize=1// 메모리를 통해 할당할 범위 사이즈
        )
@Getter @SuperBuilder @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardEntity extends BaseEntity {

    @Id @GeneratedValue(
            strategy=GenerationType.SEQUENCE, // 사용할 전략을 시퀀스로  선택
            generator="BOARD_SEQ_GEN" // 식별자 생성기를 설정해놓은  BOARD_SEQ_GEN 설정
    )
    @Column(name = "bbs_sno")
    private Long bbsSno;

    @Column(name = "title")
    private String title;

    @Column(name = "cn")
    private String cn;

    @OneToMany(mappedBy = "boardEntity")
    @Builder.Default
    private List<AnswerEntity> answers = new ArrayList<>();

}
