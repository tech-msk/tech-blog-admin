package com.tech.blog.admin.domain.dto.response;

import lombok.Data;

@Data
public class AnswerResponse {

    private String mberNm;
    private Long answerSno;
    private Long upperAnswerSno;
    private String cn;

}
