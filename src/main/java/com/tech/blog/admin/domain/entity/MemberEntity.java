package com.tech.blog.admin.domain.entity;

import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TB_MEMBER")
@Getter
public class MemberEntity {

    @Id
    @Column(name = "mber_sno")
    private Long mberSno;

    @Column(name = "mber_id")
    private String mberId;

    @Column(name = "mber_nm")
    private String mberNm;

    @Column(name = "mber_pswd")
    private String mberPswd;

    @OneToMany(mappedBy = "memberEntity")
    private List<AnswerEntity> answerEntitys = new ArrayList<>();

}
