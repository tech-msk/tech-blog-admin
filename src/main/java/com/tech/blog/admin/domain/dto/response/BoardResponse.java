package com.tech.blog.admin.domain.dto.response;

import com.tech.blog.admin.domain.model.PageWrapper;
import lombok.Data;

import java.util.List;

@Data
public class BoardResponse {

    private String mberNm;
    private String regDtm;
    private String title;
    private String cn;
    private List<String> categories;
    private List<PageWrapper<AnswerResponse>> answers;

}
