package com.tech.blog.admin.domain.entity.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass // BaseEntity를 상속한 엔티티들은 아래 필드들을 컬럼으로 인식하게 된다.
@EntityListeners(AuditingEntityListener.class) // Auditing(자동으로 값 매핑) 기능추가) , main class에 @EnableJpaAuditing 어노테이션 추가해야함
@Getter @SuperBuilder @NoArgsConstructor @AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class BaseAbstractEntity {

    @CreationTimestamp
    @Column(name = "reg_dtm", updatable = false)
    private LocalDateTime regDtm;

    @UpdateTimestamp
    @Column(name = "mdf_dtm")
    private LocalDateTime mdfDtm;

}
