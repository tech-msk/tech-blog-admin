package com.tech.blog.admin.domain.entity;

import com.tech.blog.admin.domain.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "TB_ANSWER")
@Getter
@NoArgsConstructor @AllArgsConstructor
public class AnswerEntity extends BaseEntity {

    @Id
    @Column(name = "answer_sno")
    private Long answerSno;

    @Column(name = "upper_answer_sno")
    private Long upperAnswerSno;

    @Column(name = "cn")
    private String cn;

    @ManyToOne
    @JoinColumn(name = "bbs_sno")
    private BoardEntity boardEntity;

    @ManyToOne
    @JoinColumn(name = "mber_sno")
    private MemberEntity memberEntity;

}
