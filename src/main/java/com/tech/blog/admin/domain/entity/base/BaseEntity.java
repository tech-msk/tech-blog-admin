package com.tech.blog.admin.domain.entity.base;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
@Getter @SuperBuilder @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BaseEntity extends BaseAbstractEntity {

    @Transient
    public static final Long defaultSno = 1L;
    @Transient
    public static final String defaultIpAdres = "127.0.0.1";

    @Column(name = "regpe_sno", updatable = false)
    @Builder.Default
    private Long regpeSno = defaultSno;

    @Column(name = "regpe_ip_adres", updatable = false)
    @Builder.Default
    private String regpeIpAdres = defaultIpAdres;

    @Column(name = "mdfpe_sno")
    @Builder.Default
    private Long mdfpeSno = defaultSno;

    @Column(name = "mdfpe_ip_adres")
    @Builder.Default
    private String mdfpeIpAdres = defaultIpAdres;

    @Column(name = "use_yn")
    @Builder.Default
    private String useYn = "Y";

    @Column(name = "del_yn")
    @Builder.Default
    private String delYn = "N";

    public void remove () {
        this.useYn = "N";
        this.delYn = "Y";
    }

}
