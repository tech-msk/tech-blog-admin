package com.tech.blog.admin.domain.dto.request;

import com.tech.blog.admin.domain.model.BaseRequest;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BoardRequest extends BaseRequest { }
