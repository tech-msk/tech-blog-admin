﻿-- 테이블 순서는 관계를 고려하여 한 번에 실행해도 에러가 발생하지 않게 정렬되었습니다.

DROP TABLE IF EXISTS TB_BOARD CASCADE;
DROP SEQUENCE IF EXISTS BOARD_SEQ;

CREATE SEQUENCE BOARD_SEQ START WITH 1 INCREMENT BY 1;

-- TB_BOARD Table Create SQL
CREATE TABLE TB_BOARD
(
    `regpe_sno`       LONG            NULL        COMMENT '등록자 일련번호',
    `regpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '등록자 IP 주소',
    `reg_dtm`         TIMESTAMP       NULL        COMMENT '등록 일자',
    `mdfpe_sno`       LONG            NULL        COMMENT '수정자 일련번호',
    `mdfpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '수정자 IP 주소',
    `mdf_dtm`         TIMESTAMP       NULL        COMMENT '수정 일자',
    `use_yn`          CHAR(1)         NULL        COMMENT '사용 여부',
    `del_yn`          CHAR(1)         NULL        COMMENT '삭제 여부',
    `bbs_sno`         LONG            NULL        COMMENT '게시판 일련번호',
    `title`           VARCHAR2(20)    NULL        COMMENT '제목',
    `cn`              TEXT            NULL        COMMENT '내용',
    PRIMARY KEY (`bbs_sno`)
);

-- ALTER TABLE TB_BOARD COMMENT '게시판';


DROP TABLE IF EXISTS TB_CATEGORY CASCADE;
DROP SEQUENCE IF EXISTS CATEGORY_SEQ;

CREATE SEQUENCE CATEGORY_SEQ START WITH 1 INCREMENT BY 1;
-- TB_CATEGORY Table Create SQL
CREATE TABLE TB_CATEGORY
(
    `regpe_sno`        LONG            NULL        COMMENT '등록자 일련번호',
    `regpe_ip_adres`   VARCHAR2(15)    NULL        COMMENT '등록자 IP 주소',
    `reg_dtm`          TIMESTAMP       NULL        COMMENT '등록 일자',
    `mdfpe_sno`        LONG            NULL        COMMENT '수정자 일련번호',
    `mdfpe_ip_adres`   VARCHAR2(15)    NULL        COMMENT '수정자 IP 주소',
    `mdf_dtm`          TIMESTAMP       NULL        COMMENT '수정 일자',
    `use_yn`           CHAR(1)         NULL        COMMENT '사용 여부',
    `del_yn`           CHAR(1)         NULL        COMMENT '삭제 여부',
    `ctgry_sno`        LONG            NULL        COMMENT '카테고리 일련번호',
    `ctgry_nm`         VARCHAR2(20)    NULL        COMMENT '카테고리 이름',
    `upper_ctgry_sno`  LONG            NULL        COMMENT '상위 카테고리 일련번호',
    PRIMARY KEY (`ctgry_sno`)
);

--ALTER TABLE TB_CATEGORY COMMENT '카테고리';

DROP TABLE IF EXISTS TB_MEMBER CASCADE;
DROP SEQUENCE IF EXISTS TB_MEMBER;

CREATE SEQUENCE TB_MEMBER START WITH 1 INCREMENT BY 1;
-- TB_MEMBER Table Create SQL
CREATE TABLE TB_MEMBER
(
    `regpe_sno`       LONG            NULL        COMMENT '등록자 일련번호',
    `regpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '등록자 IP 주소',
    `reg_dtm`         TIMESTAMP       NULL        COMMENT '등록 일자',
    `mdfpe_sno`       LONG            NULL        COMMENT '수정자 일련번호',
    `mdfpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '수정자 IP 주소',
    `mdf_dtm`         TIMESTAMP       NULL        COMMENT '수정 일자',
    `use_yn`          CHAR(1)         NULL        COMMENT '사용 여부',
    `del_yn`          CHAR(1)         NULL        COMMENT '삭제 여부',
    `mber_sno`        LONG            NOT NULL    COMMENT '회원 일련번호',
    `mber_id`         VARCHAR2(20)    NOT NULL    COMMENT '회원 아이디',
    `mber_nm`         VARCHAR2(20)    NULL        COMMENT '회원 이름',
    `mber_pswd`       VARCHAR(20)     NULL        COMMENT '회원 비밀번호',
    PRIMARY KEY (`mber_sno`)
);

--ALTER TABLE TB_MEMBER COMMENT '회원';

DROP TABLE IF EXISTS TB_BOARD_CATEGORY_MPNG CASCADE;

-- TB_BOARD_CATEGORY_MPNG Table Create SQL
CREATE TABLE TB_BOARD_CATEGORY_MPNG
(
    `regpe_sno`       LONG            NULL        COMMENT '등록자 일련번호',
    `regpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '등록자 IP 주소',
    `reg_dtm`         TIMESTAMP       NULL        COMMENT '등록 일자',
    `mdfpe_sno`       LONG            NULL        COMMENT '수정자 일련번호',
    `mdfpe_ip_adres`  VARCHAR2(15)    NULL        COMMENT '수정자 IP 주소',
    `mdf_dtm`         TIMESTAMP       NULL        COMMENT '수정 일자',
    `use_yn`          CHAR(1)         NULL        COMMENT '사용 여부',
    `del_yn`          CHAR(1)         NULL        COMMENT '삭제 여부',
    `bbs_sno`         LONG            NULL        COMMENT '게시판 일련번호',
    `ctgry_sno`       LONG            NULL        COMMENT '카테고리 일련번호'
);

--ALTER TABLE TB_BOARD_CATEGORY_MPNG COMMENT '게시판카테고리매핑';

ALTER TABLE TB_BOARD_CATEGORY_MPNG
    ADD CONSTRAINT FK_TB_BOARD_CATEGORY_MPNG_bbs_sno_TB_BOARD_bbs_sno FOREIGN KEY (`bbs_sno`)
        REFERENCES TB_BOARD (`bbs_sno`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE TB_BOARD_CATEGORY_MPNG
    ADD CONSTRAINT FK_TB_BOARD_CATEGORY_MPNG_ctgry_sno_TB_CATEGORY_ctgry_sno FOREIGN KEY (`ctgry_sno`)
        REFERENCES TB_CATEGORY (`ctgry_sno`) ON DELETE RESTRICT ON UPDATE RESTRICT;

DROP TABLE IF EXISTS TB_ANSWER CASCADE;
DROP SEQUENCE IF EXISTS ANSWER_SEQ;

-- TB_ANSWER Table Create SQL
CREATE SEQUENCE ANSWER_SEQ START WITH 1 INCREMENT BY 1;
CREATE TABLE TB_ANSWER
(
    `regpe_sno`         LONG            NULL        COMMENT '등록자 일련번호',
    `regpe_ip_adres`    VARCHAR2(15)    NULL        COMMENT '등록자 IP 주소',
    `reg_dtm`           TIMESTAMP       NULL        COMMENT '등록 일자',
    `mdfpe_sno`         LONG            NULL        COMMENT '수정자 일련번호',
    `mdfpe_ip_adres`    VARCHAR2(15)    NULL        COMMENT '수정자 IP 주소',
    `mdf_dtm`           TIMESTAMP       NULL        COMMENT '수정 일자',
    `use_yn`            CHAR(1)         NULL        COMMENT '사용 여부',
    `del_yn`            CHAR(1)         NULL        COMMENT '삭제 여부',
    `answer_sno`        LONG            NULL        COMMENT '댓글 일련번호',
    `cn`                TEXT            NULL        COMMENT '내용',
    `upper_answer_sno`  LONG            NULL        COMMENT '상위 댓글 일련번호',
    `bbs_sno`           LONG            NULL        COMMENT '게시판 일련번호',
    PRIMARY KEY (`answer_sno`)
);

--ALTER TABLE TB_ANSWER COMMENT '댓글';

ALTER TABLE TB_ANSWER
    ADD CONSTRAINT FK_TB_ANSWER_bbs_sno_TB_BOARD_bbs_sno FOREIGN KEY (`bbs_sno`)
        REFERENCES TB_BOARD (`bbs_sno`) ON DELETE RESTRICT ON UPDATE RESTRICT;


